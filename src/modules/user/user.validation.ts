import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { IUser, IUpdateUser } from "../../types/user.types.js";
import { InvalidArgumentsError } from "../../exceptions/invalidArguments.exception.js";
export const CREATE_USER = "CREATE_USER";
export const UPDATE_USER = "UPDATE_USER";

const validateUser = async (user: IUser | IUpdateUser, type = UPDATE_USER) => {
    let first_name = Joi.string().alphanum().min(3).max(30);
    let last_name = Joi.string().alphanum().min(3).max(30);
    let email = Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    });
    let phone = Joi.string().pattern(new RegExp("^[0-9]+$")).length(10);

    if (type == CREATE_USER) {
        first_name = first_name.required();
        last_name = last_name.required();
        email = email.required();
        phone = phone.required();
    }
    const schema = Joi.object().keys({ first_name, last_name, email, phone });

    try {
        return await schema.validateAsync(user);
    } catch (err: any) {
        throw new InvalidArgumentsError(err.details[0].message, 404);
    }
};

export const validationFactoryMethod = (type: string) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await validateUser(req.body, type);
        next();
    };
};
