import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
  first_name: {
    type: String,
    required: [true, "First name is required"],
    minLength: 3,
    maxLength: 30,
  },
  last_name: {
    type: String,
    required: [true, "Last name is required"],
    minLength: 3,
    maxLength: 30,
  },
  email: { type: String, required: [true, "Email is required"] },
  phone: {
    type: String,
    required: [true, "Phone number is required"],
    minLength: 10,
    maxLength: 10,
  },
});

export default model("user", UserSchema);
