import { HttpError } from "./http.exception.js";

export class InvalidArgumentsError extends HttpError {
  constructor(message:string, status:number) {
    super(message, status);
  }
}
