import { HttpError } from "./http.exception.js";

export class InstanceNotFoundError extends HttpError {
  constructor(message:string, status:number) {
    super(message, status);
  }
}
