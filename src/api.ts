import express, { Express } from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./api-routes/user.router.js";
import {
    printError,
    errorResponse,
    not_found,
    errorLogger,
} from "./middleware/errors.handler.js";

class App {
    private readonly PORT: number;
    private readonly HOST: string;
    private readonly DB_URI: string;
    private readonly app: Express;

    constructor() {
        const { PORT = 8080, HOST = "localhost", DB_URI } = process.env;
        this.PORT = Number(PORT);
        this.HOST = String(HOST);
        this.DB_URI = String(DB_URI);
        this.app = express();
    }

    initMiddlewares() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
    }

    initRoutingFunctions() {
        this.app.use("/api/users", user_router);
    }

    handleErrors() {
        this.app.use(printError);
        this.app.use(errorLogger("./loggers/errorLogger"));
        this.app.use(errorResponse);
    }

    handleNoMatchingRoutes() {
        this.app.use("*", not_found);
    }

    async connectServer() {
        try {
            await connect_db(this.DB_URI);
            await this.app.listen(Number(this.PORT), this.HOST as string, () =>
                log.magenta(
                    `api is live on`,
                    ` ✨ ⚡  http://${this.HOST}:${this.PORT} ✨ ⚡`
                )
            );
        } catch (err) {
            console.log(err);
        }
    }

    async startServer() {
        this.initMiddlewares();
        this.initRoutingFunctions();
        this.handleErrors();
        this.handleNoMatchingRoutes();
        await this.connectServer();
    }
}

const app = new App();
app.startServer();
