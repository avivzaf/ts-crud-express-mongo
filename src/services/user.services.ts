import { userDB } from "../db/models/user.db.js";
import { IUpdateUser, IUser } from "../types/user.types.js";

class UserService {
    createNewUser = async (userDetails: IUser) =>
        await userDB.createNewUser(userDetails);

    getAllUsers = async () => await userDB.getAllUsers();

    getUsersFromPage = async (page: number, numberOfUsers: number) =>
        await userDB.getUsersFromPage(page, numberOfUsers);

    getSingleUser = async (id: string) => await userDB.getSingleUser(id);

    updateUser = async (id: string, userDetails:IUpdateUser) =>
        await userDB.updateUser(id, userDetails);

    deleteUser = async (id: string) => await userDB.deleteUser(id);
}

export const userService = new UserService();
