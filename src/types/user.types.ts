export interface IUser {
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
}

export type IUpdateUser = Partial<IUser>;
