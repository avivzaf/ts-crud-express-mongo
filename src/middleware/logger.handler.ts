import fs from "fs";
import pkg from "uuid";
import { Request, Response, NextFunction } from "express";

export function logger(filePath: string) {
    const writeStream = fs.createWriteStream(filePath, {
        encoding: "utf-8",
        flags: "a",
    });

    return function (req: Request, res: Response, next: NextFunction) {
        const { method, originalUrl } = req;
        const { v4: uuid } = pkg;

        req.id = uuid();
        writeStream.write(
            `${req.id} ${method} :: ${originalUrl} >> ${Math.floor(
                Date.now() / 1000
            )}\n`
        );

        next();
    };
}
