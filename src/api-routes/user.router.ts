import raw from "../utils/route.async.wrapper.js";
import express from "express";
import {
  CREATE_USER,
  UPDATE_USER,
  validationFactoryMethod,
} from "../modules/user/user.validation.js";
import { userController } from "../controllers/user.controllers.js";
import { logger } from "../middleware/logger.handler.js";

const router = express.Router();

router.use(express.json());
router.use(logger("./loggers/requestLogger"));

router.post("/", raw(validationFactoryMethod(CREATE_USER)), raw(userController.createNewUser));
router.get("/", raw(userController.getAllUser));
router.get("/:page/:numberOfUsers", raw(userController.getUsersFromPage));
router.get("/:id", raw(userController.getSingleUser));
router.put("/:id", raw(validationFactoryMethod(UPDATE_USER)), raw(userController.updateUser));
router.delete("/:id", raw(userController.deleteUser));

export default router;
