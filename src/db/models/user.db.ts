import user_model from "../../modules/user/user.model.js";
import { IUpdateUser, IUser } from "../../types/user.types.js";

class UserDB {
    getUsersFromPage = async (page: number, numberOfUsers: number) =>
        await user_model
            .find()
            .skip((page - 1) * 20)
            .limit(numberOfUsers);

    getAllUsers = async () =>
        await user_model.find().select(`-_id 
        first_name 
        last_name 
        email 
        phone`);

    createNewUser = async (userDetails: IUser) =>
        await user_model.create(userDetails);

    getSingleUser = async (id: string) => await user_model.findById(id);

    updateUser = async (id: string, userDetails: IUpdateUser) =>
        await user_model.findByIdAndUpdate(id, userDetails, {
            new: true,
            upsert: false,
        });

    deleteUser = async (id: string) => await user_model.findByIdAndRemove(id);
}

export const userDB = new UserDB();
